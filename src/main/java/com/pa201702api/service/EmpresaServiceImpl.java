package com.pa201702api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pa201702api.dto.EmpresaDTO;
import com.pa201702api.model.Empresa;
import com.pa201702api.repository.EmpresaRepository;

@Component
public class EmpresaServiceImpl implements EmpresaService {
	
	@Autowired
	private EmpresaRepository empresaRepository;
	
	@Override
	public void salvarEmpresa(EmpresaDTO empresaDTO) {
		Empresa empresa = new Empresa();
		empresa.setNome(empresaDTO.getNome());
		empresa.setEmail(empresaDTO.getEmail());
		empresaRepository.save(empresa);
	}

}
