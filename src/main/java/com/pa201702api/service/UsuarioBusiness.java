package com.pa201702api.service;

import java.util.List;

import com.pa201702api.dto.UsuarioDTO;
import com.pa201702api.model.Usuario;
import com.pa201702api.repository.UsuarioRepository;
import java.util.function.Consumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

@Component
public class UsuarioBusiness {

    @Autowired
    private UsuarioRepository usuarioRepository;

    public void salvarUsuario(UsuarioDTO usuarioDTO) {
        Usuario usuario = new Usuario();
        usuario.setEmail(usuarioDTO.getEmail());
        usuario.setSenha(usuarioDTO.getSenha());
        usuarioRepository.save(usuario);
    }

    public List ListarUsuarios() {
        return (List) usuarioRepository.findAll();
    }

    public Usuario buscaUsuario(Long id) {
        Usuario usuario = new Usuario();
        usuario.setId(id);
        return usuarioRepository.findOne(usuario.getId());
    }

}
