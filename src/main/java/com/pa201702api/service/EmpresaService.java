package com.pa201702api.service;

import com.pa201702api.dto.EmpresaDTO;

public interface EmpresaService {
	
	void salvarEmpresa(EmpresaDTO empresaDTO);

}
