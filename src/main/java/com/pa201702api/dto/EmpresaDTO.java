package com.pa201702api.dto;

public class EmpresaDTO extends BaseDTO {
	
	private String nome;
	
	private String email;
	
	public EmpresaDTO() {
	}

	public EmpresaDTO(String nome, String email) {
		super();
		this.nome = nome;
		this.email = email;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;	
	}

}
