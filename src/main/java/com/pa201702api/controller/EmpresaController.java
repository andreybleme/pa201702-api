package com.pa201702api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pa201702api.dto.EmpresaDTO;
import com.pa201702api.service.EmpresaService;

@RestController
public class EmpresaController {
	
	@Autowired
	private EmpresaService empresaService;
	
	@RequestMapping(path = "/empresa", method = RequestMethod.POST)
	public ResponseEntity<EmpresaDTO> criarEmpresa(@RequestBody EmpresaDTO empresaDTO) {
		try {
			empresaService.salvarEmpresa(empresaDTO);
			return new ResponseEntity<EmpresaDTO>(empresaDTO, HttpStatus.OK);
		} catch (Exception e) {
			empresaDTO.setMensagem(e.getMessage());
			return new ResponseEntity<EmpresaDTO>(empresaDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
