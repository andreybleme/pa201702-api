package com.pa201702api.controller;

import java.util.List;

import com.pa201702api.dto.UsuarioDTO;
import com.pa201702api.model.Usuario;
import com.pa201702api.service.UsuarioServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsuarioController {

    @Autowired
    private UsuarioServiceImpl usuarioBusiness;

    @RequestMapping(path = "/usuario", method = RequestMethod.POST)
    public ResponseEntity<UsuarioDTO> cadastrarUsuario(@RequestBody UsuarioDTO usuarioDTO) {
        try {
            usuarioBusiness.salvarUsuario(usuarioDTO);
            return new ResponseEntity<UsuarioDTO>(usuarioDTO, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<UsuarioDTO>(usuarioDTO, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @RequestMapping(value = "/usuario/", method = RequestMethod.GET)
    public ResponseEntity<List<UsuarioDTO>> listarTodosUsuários() {
        List<UsuarioDTO> lista = usuarioBusiness.ListarUsuarios();
        if (lista.isEmpty()) {
            return new ResponseEntity<List<UsuarioDTO>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<UsuarioDTO>>(lista, HttpStatus.OK);
    }

    @RequestMapping(value = "/usuario/{id}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<UsuarioDTO> getUsuario(@PathVariable("id") long id) {
        System.out.println("Fetching User with id " + id);
        Usuario usuario = usuarioBusiness.buscaUsuario(id);
        if (usuario == null) {
            System.out.println("User with id " + id + " not found");
            return new ResponseEntity<UsuarioDTO>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<UsuarioDTO>((MultiValueMap<String, String>) usuario, HttpStatus.OK);
    }

}
