package com.pa201702api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class Pa201702ApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(Pa201702ApiApplication.class, args);
	}
}
