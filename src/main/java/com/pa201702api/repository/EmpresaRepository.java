package com.pa201702api.repository;

import org.springframework.data.repository.CrudRepository;

import com.pa201702api.model.Empresa;

public interface EmpresaRepository extends CrudRepository<Empresa, Long> {

}
