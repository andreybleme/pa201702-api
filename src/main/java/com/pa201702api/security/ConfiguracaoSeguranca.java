/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pa201702api.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
/**
 *
 * @author Jardiano
 */

@Configuration
@EnableWebSecurity
public class ConfiguracaoSeguranca extends WebSecurityConfigurerAdapter{
    private static String REALM="MY_TEST_REALM";
    
    @Autowired
    public void configuraSegurancaGlobal(AuthenticationManagerBuilder auth) throws Exception{
        auth.inMemoryAuthentication().withUser("teste").password("teste123").roles("ADMIN");
    }
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/usuario/**").hasAnyRole("USUARIO","ADMIN")
                .and().httpBasic().realmName(REALM).authenticationEntryPoint(getAutenticacaoEntryPoint())
                .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED);
    }
    
    @Bean
    public AutenticacaoEntryPoint getAutenticacaoEntryPoint(){
        return new AutenticacaoEntryPoint();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
           web.ignoring().antMatchers(HttpMethod.OPTIONS,"/**");
    }
    
    
    
}
